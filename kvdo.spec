#This spec is obtained from source code(kvdo-6.2.2.24.tar.gz)
%define spec_release 6
%define kmod_name		kmod-kvdo
%define kmod_driver_version	8.2.1.2
%define kmod_rpm_release	%{spec_release}
%define kmod_kernel_version	3.10.0-693.el7

# Disable the scanning for a debug package
%global debug_package %{nil}

Name:		kmod-kvdo
Version:	%{kmod_driver_version}
Release:	%{kmod_rpm_release}
Summary:	Kernel Modules for Virtual Data Optimizer
License:	GPLv2+
URL:		http://github.com/dm-vdo/kvdo
Source0:        https://github.com/dm-vdo/kvdo/archive/refs/tags/%{kmod_driver_version}.tar.gz
Patch1:         01-add-riscv64-support.patch
Patch2:         0001-add-loongarch64-support-for-kmod-kvdo.patch
Patch3:         0002-replace-kernel-obsolete-api.patch
Patch4:         0003-add-Lz4_compress-for-x86_64-arch.patch
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

Requires:       dkms
Requires:	kernel-devel >= %{kmod_kernel_version}
Requires:       make
ExclusiveArch:	x86_64 aarch64 loongarch64 riscv64 ppc64le
ExcludeArch:	s390 s390x ppc ppc64 i686

%description
Virtual Data Optimizer (VDO) is a device mapper target that delivers
block-level deduplication, compression, and thin provisioning.

This package provides the kernel modules for VDO.

%post
set -x
/usr/sbin/dkms --rpm_safe_upgrade add -m %{kmod_name} -v %{version}-%{kmod_rpm_release}
/usr/sbin/dkms --rpm_safe_upgrade build -m %{kmod_name} -v %{version}-%{kmod_rpm_release}
/usr/sbin/dkms --rpm_safe_upgrade install -m %{kmod_name} -v %{version}-%{kmod_rpm_release}

%preun
# Check whether kvdo or uds is loaded, and if so attempt to remove it.  A
# failure here means there is still something using the module, which should be
# cleared up before attempting to remove again.
for module in kvdo uds; do
  if grep -q "^${module}" /proc/modules; then
    modprobe -r ${module}
  fi
done
/usr/sbin/dkms --rpm_safe_upgrade remove -m %{kmod_name} -v %{version}-%{kmod_rpm_release} --all || :

%prep
%setup -n kvdo-%{kmod_driver_version}
%patch -P1 -p1
%patch -P2 -p1
%patch -P3 -p1
%ifarch x86_64
%patch -P4 -p1
%endif

%build
# Nothing doing here, as we're going to build on whatever kernel we end up
# running inside.

%install
mkdir -p $RPM_BUILD_ROOT/%{_usr}/src/%{kmod_name}-%{version}-%{kmod_rpm_release}
cp -r * $RPM_BUILD_ROOT/%{_usr}/src/%{kmod_name}-%{version}-%{kmod_rpm_release}/
cat > $RPM_BUILD_ROOT/%{_usr}/src/%{kmod_name}-%{version}-%{kmod_rpm_release}/dkms.conf <<EOF
PACKAGE_NAME="kvdo"
PACKAGE_VERSION="%{version}-%{kmod_rpm_release}"
AUTOINSTALL="yes"

BUILT_MODULE_NAME[0]="kvdo"
BUILT_MODULE_LOCATION[0]="vdo"
DEST_MODULE_LOCATION[0]="/kernel/drivers/block/"
BUILD_DEPENDS[0]=LZ4_COMPRESS
BUILD_DEPENDS[0]=LZ_DECOMPRESS
STRIP[0]="no"
EOF

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(644,root,root,755)
%{_usr}/src/%{kmod_name}-%{version}-%{kmod_rpm_release}/*

%changelog
* Wed Feb 12 2025 Ge Wang <wang__ge@126.com> - 8.2.1.2-6
- Replace kernel obsolete api andd add Lz4_compress interface for x86_64 arch

* Thu Dec 12 2024 wangshuo <wangshuo@kylinos.cn> - 8.2.1.2-5
- vdo/cpu.h: add loongarch64 support, fix dkms build error

* Fri Mar 8 2024 xurui <rui.xu@shingroup.cn> - 8.2.1.2-4
- Add ppc64le support

* Wed Apr 12 2023 laokz <zhangkai@iscas.ac.cn> - 8.2.1.2-3
- Add riscv64 support

* Thu Feb 16 2023 Wenlong Zhang<zhangwenlong@loongson.cn> - 8.2.1.2-2
- Add loongarch64 support

* Thu Feb 02 2023 Ge Wang <wangge20@huawei.com> - 8.2.1.2-1
- Upgrade kmod-kvdo to 8.2.1.2

* Wed Dec 29 2021 yaoxin <yaoxin30@huawei.com> - 8.1.0.316-1
- Upgrade kmod-kvdo to 8.1.0.316

* Wed Nov 11 2020 wutao <wutao61@huawei.com> - 6.2.3.114-1
- Update to 6.2.3.114 and adapt to kernel 5.10

* Fri Aug 28 2020 sunguoshuai <sunguoshuai@huawei.com> - 6.2.2.24-6
- Modify the install dir.

* Sun Jan 19 2020 sunguoshuai <sunguoshuai@huawei.com> - 6.2.2.24-5
- Modify source0 from github.

* Thu Dec 12 2019 openEuler Buildteam <buildteam@openeuler.org> -6.2.2.24-4
- Modify source0

* Thu Dec 12 2019 openEuler Buildteam <buildteam@openeuler.org> -6.2.2.24-3
- Add support for aarch64

* Tue Dec 3 2019 openEuler Buildteam <buildteam@openeuler.org> -6.2.2.24-2
- Modify release
